import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CoilPage extends StatefulWidget {
  @override
  _CoilPageState createState() => _CoilPageState();
}

class _CoilPageState extends State<CoilPage> {
  String info =
      "Coil adalah sebuah kawat atau wire yang memiliki hambatan untuk memanaskan sebuah kapas yang di basahi oleh liquid atau cairan vape yang akan menghasilkan uap";

  List<ListInfo> data = [
    ListInfo(
      title: "Round wire",
      image: "assets/coil/coil1.jpg",
      contentInfo:
          "Round wire adalah kawat yang memiliki ukuran diameter atau biasa di sebut AWG semakin kecil awg maka semakin kecil resistance atau hambatanya,round wire ada berbagai jenis yaitu kantal,nicroum dan stainless",
    ),
    ListInfo(
      title: "Prebuild",
      image: "assets/coil/coil2.jpg",
      contentInfo:
          "Prebuild adalah lawat atau wire yg telah di susun menjadi kombinasi lilitan ada beberapa jenis prebuild yaitu ,twisted,fused calpten,alien clapton,dan mesh",
    ),
    ListInfo(
      title: "Occ",
      image: "assets/coil/coil3.png",
      contentInfo:
          "OCC adalah coil pabrikan yang dikhususkan untuk penguna pods atau aio yang tinggal plug and play (pnp)",
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height / 3.5,
                width: MediaQuery.of(context).size.width,
                color: Color(0xff183D60),
                child: Stack(
                  children: [
                    Align(
                      alignment: AlignmentDirectional.topStart,
                      child: GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 20,
                      right: 40,
                      child: Image.asset(
                        "assets/vape/coilfull.png",
                        height: MediaQuery.of(context).size.height / 4.5,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 7,
                    ),
                    Text(
                      "COIL",
                      style: TextStyle(
                        color: Colors.white,
                        letterSpacing: 1,
                        fontSize: 20,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 30,
                          vertical: 30,
                        ),
                        child: Text(
                          info,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      itemCount: data.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        return data[index].content();
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ListInfo {
  final String title;
  final String image;
  final String contentInfo;

  ListInfo({this.title, this.contentInfo, this.image});

  Widget content() {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Image.asset(
                image,
                width: 100,
                fit: BoxFit.contain,
              ),
            ),
          ),
          Expanded(
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  20,
                ),
              ),
              child: Container(
                padding: EdgeInsets.symmetric(
                  vertical: 20,
                  horizontal: 20,
                ),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: 10),
                    Text(
                      contentInfo,
                      style: TextStyle(
                        fontSize: 12,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
