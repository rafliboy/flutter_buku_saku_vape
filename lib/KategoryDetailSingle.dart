import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:uts/DetailKategorySingle.dart';

class KategoryDetailSingle extends StatefulWidget {
  @override
  _KategoryDetailSingleState createState() => _KategoryDetailSingleState();
}

class _KategoryDetailSingleState extends State<KategoryDetailSingle> {
  String info =
      "Mod adalah tempat penyimpanan baterai serta komponen-komponen pendukung vape lainnya. Memiliki bentuk kotak dan juga tabung. Jika dilihat dari kegunaannya maka mod vape memiliki dua jenis mod yaitu mechanical mod dan electrical mod.";

  // list menu info model
  List<MenuInfo> menu = [
    MenuInfo(
      title: "Mechanical",
      image: "assets/img/mechanical.png",
      link: "https://www.youtube.com/watch?v=IToAM5S1_ok",
      info:
          "Mechanical Mod adalah mod yang murni hanya berfungsi sebagai penghantar listrik dari baterai ke atomizer, artinya sama sekali tidak ada komponen listrik yang menempel pada body/mod. Kelebihan dari mechanical mod yaitu dapat meningkatkan performa atomizer di genesis style dan kekurangannya adalah performa atomizer sangat bergantung pada kekuatan bateri,",
    ),
    MenuInfo(
      title: "Eletrical",
      image: "assets/img/electrical.png",
      link: "https://www.youtube.com/watch?v=t4kwceDV_L4",
      info:
          "electrical mod memiliki komponen listrik yang menempel pada body. Komponen listrik ini berfungsi untuk mengganti volt yang diinginkan atau mematikan vape saat tidak digunakan. Electrical mod juga dilengkapi dengan layar mini untuk melihat performa baterai dan hal lainnya. Kelebihan dari electrical mod adalah kamu dapat mengatur volt sesukamu dan mengetahui informasi mod dengan akurat sementara kekurangannya adalah mod tidak dapat digunakan kembali apabila terdapat chip yang rusak. ",
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height / 3.5,
                width: MediaQuery.of(context).size.width,
                color: Color(0xff183D60),
                child: Stack(
                  children: [
                    Align(
                      alignment: AlignmentDirectional.topStart,
                      child: GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 20,
                      right: 30,
                      child: Image.asset(
                        "assets/vape/modsfull.png",
                        height: MediaQuery.of(context).size.height / 4.5,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 7,
                    ),
                    Text(
                      "MODS",
                      style: TextStyle(
                        color: Colors.white,
                        letterSpacing: 1,
                        fontSize: 20,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Container(
                        constraints: BoxConstraints(minHeight: 200),
                        padding: EdgeInsets.symmetric(
                          horizontal: 30,
                          vertical: 30,
                        ),
                        child: Center(
                          child: Text(
                            info,
                            style: TextStyle(
                              fontSize: 16,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        // menu info index ke 0
                        menu[0].cardMenu(),
                        SizedBox(width: 10),
                        // menu info index ke 1
                        menu[1].cardMenu(),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class MenuInfo {
  final String title;
  final String image;
  final String info;
  final String link;
  MenuInfo({this.title, this.image, this.info, this.link});

  Widget cardMenu() {
    return Expanded(
      child: GestureDetector(
        onTap: () {
          Get.to(
            DetailKategorySingle(
              title: title,
              info: info,
              link: link,
            ),
          );
        },
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            child: Column(
              children: [
                Image.asset(
                  image,
                  width: 100,
                  height: 80,
                ),
                SizedBox(height: 20),
                Text(
                  title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
