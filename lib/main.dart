import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:uts/Splashscreen.dart';
import 'Home/Home.dart';

//Memanggil class list yang akan di pangil mealalui on tap
void main() => runApp(MyApp());
//Main Utama Memanggil MyApp

class MyApp extends StatelessWidget {
  final appTitle = 'APP RAFLI ';
  //app di beri judul app Rafli
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      // menu awal akan diredirect ke splashscreen
      home: Splashscreen(),
    );
  }
}
