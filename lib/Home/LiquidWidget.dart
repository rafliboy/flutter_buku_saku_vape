import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:uts/model/LiquidModel.dart';

import '../DetailRd.dart';
import '../LiquidPage.dart';

class LiquidWidget extends StatelessWidget {
  // bikin data list liquid
  List<LiquidModel> liquid = [
    LiquidModel(
      title: "Creamy",
      image: "assets/liquid/liquid1.jpg",
      deskripsi:
          "Rasa liquid ini memiliki banyk peminatnya, dikarenkn rasanya yang memang enak, manis dan tersedia dengan bebagai varian rasa, seperti coklat, strawberry, mangga dan lain sebagainya. Dan juga rasa liquid ini juga sangat cocok atau familiar dengan lidah orang Indonesia.",
    ),
    LiquidModel(
      title: "Fruity",
      image: "assets/liquid/liquid2.jpeg",
      deskripsi:
          "Dari namanya kita sudah bisa menebak bahwa liquid ini memiliki rasa buah buahan seperti melon, mangga, anggur, jeruk dan lain sebaginya. Dan untuk harganya liquid ini lebih murah dibandingkan dengan liqiud creamy, tetapi kalau fruitnya produksi dari US seperti contohnya Naked harganya lebih mahal.",
    ),
    LiquidModel(
      title: "JUICY",
      image: "assets/liquid/liquid3.jpg",
      deskripsi:
          "Liquid juicy tidak jauh beda dengan fruity yaitu memiliki citra rasa buah-buahan segar dan mengandung chiler untuk rasa dinginya. Biasanya juicy menghasilkan rasa buah-buahan yang telah dicampur. Seperti kiwi-berry, melon-bery, peach-kiwi, jeruk-sitrus, dan masih banyak lagi.",
    ),
    LiquidModel(
      title: "TOBACCO (RASA ROKOK)",
      image: "assets/liquid/liquid4.jpg",
      deskripsi:
          "Liquid jenis ini akan memberikan sensasi seperti menghisap tembakau. Ya, liquid tobacco merupakan liquid yang memiliki rasa tembakau. Liquid jenis tobacco yang paling terkenal ialah vanila crustad, caramel, dan tobacco strawberry.",
    ),
  ];

  Widget liquidPanel(int index) {
    return GestureDetector(
      onTap: () {
        Get.to(DetailRd(
          title: liquid[index].title,
          info: liquid[index].deskripsi,
          image: liquid[index].image,
        ));
      },
      child: Container(
        width: 200,
        margin: EdgeInsets.only(right: 10),
        child: Card(
          margin: EdgeInsets.only(right: 0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          child: Stack(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.asset(
                  liquid[index].image,
                  width: 200,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                width: 200,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      stops: [
                        0.5,
                        1,
                      ],
                      colors: [Colors.black.withOpacity(0), Colors.black],
                    )),
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text(
                      liquid[index].title,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 36),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(right: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Liquid",
                  style: TextStyle(
                    color: Color(0xffc7c7c7),
                    fontWeight: FontWeight.w700,
                    letterSpacing: 1,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Get.to(LiquidPage());
                  },
                  child: Text(
                    "Show all",
                    style: TextStyle(
                      color: Color(0xffc7c7c7),
                      fontWeight: FontWeight.w700,
                      letterSpacing: 1,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 180,
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemCount: liquid.length,
              itemBuilder: (BuildContext _, int index) {
                return liquidPanel(index);
              },
            ),
          ),
        ],
      ),
    );
  }
}
