import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../CoilPage.dart';
import '../DetailKategoryRd.dart';
import '../KategoryDetail.dart';
import '../KategoryDetailSingle.dart';

// bikin model menu
class Menu {
  final String title;
  final String image;
  final Widget page;
  Menu({this.title, this.image, this.page});
}

class KategoryWidget extends StatelessWidget {
  // data list kategory
  // dan bagian page akan pergi ke page tersebut
  List<Menu> kategoryMenu = [
    Menu(
      title: "Mods",
      image: "assets/vape/mods.png",
      page: KategoryDetailSingle(),
    ),
    Menu(
      title: "Pods",
      image: "assets/vape/pods.png",
      page: KategoryDetail(
        active: 0,
        title: "PODS",
        image: "assets/vape/podsfull.png",
        info:
            "pod device atau biasa di sebut close system diciptakan untuk perokok yang ingin beralih ke vape atau sebagai second device (device kedua) para vapers. Oleh karena itu, pod device awalnya didesain untuk konsentrasi nikotin yang lebih tinggi Pada pod device serta di bagi menjadi dua yaitu body yang di dalamnya terdapat battrei serta catridge yang biasanya coil-nya menyatu dengan catridge dan cara memakainya yaitu dengan MTL(MOUTH TO LOUGH) atau seprti cara merokok konfesional",
      ),
    ),
    Menu(
      title: "Aio",
      image: "assets/vape/AIO.png",
      page: KategoryDetail(
        active: 1,
        title: "AIO",
        image: "assets/vape/aiofull.png",
        info:
            "AIO (ALL IN ONE) adalah sebuah vape mungil yang hadir dalam desain satu atau dua bagian yang sederhana. AIO biasanya di desain menjadi dua bagian, bagian bawah menampung baterai bawaan, dan bagian atas menjadi bagian tank atau biasa disebut cartridge. AIO bisa menggunkan dua model cara penghisapan yaitu MTL(Mount to loungh) dan DTL (DIrect to Loungh).",
      ),
    ),
    Menu(
      title: "Automizer",
      image: "assets/vape/automizer.png",
      page: DetailKategoryRd(),
    ),
    Menu(
      title: "Coil",
      image: "assets/vape/coil.png",
      page: CoilPage(),
    ),
  ];

  Widget kategoryPanel(int index) {
    return GestureDetector(
      onTap: () {
        Get.to(kategoryMenu[index].page);
      },
      child: Container(
        padding: const EdgeInsets.only(right: 15),
        width: 90,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 60,
              child: Image.asset(
                kategoryMenu[index].image,
                fit: BoxFit.contain,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 50,
              child: Text(
                kategoryMenu[index].title,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  letterSpacing: 1,
                  color: Color(0xff404040),
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 36),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Kategory",
            style: TextStyle(
              color: Color(0xffc7c7c7),
              fontWeight: FontWeight.w700,
              letterSpacing: 1,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 120,
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemCount: kategoryMenu.length,
              itemBuilder: (BuildContext _, int index) {
                return kategoryPanel(index);
              },
            ),
          ),
        ],
      ),
    );
  }
}
