import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:uts/model/MarkerMapsModel.dart';

import '../Maps.dart';

class StoreWidget extends StatelessWidget {
  //bikin Maps Model nya
  //model diambil difolder model/markermapsmodel
  List<MarkerMapsModel> vapeStore = [
    MarkerMapsModel(
      lat: -7.7575629205656815,
      long: 110.39602435952017,
      title: "Philovapor",
      img: "assets/store/store1.jpg",
      deskripsi:
          "Jl. Anggajaya 1 No.302, RW.2, Sanggrahan, Condongcatur, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55283",
    ),
    MarkerMapsModel(
      lat: -7.773471888545401,
      long: 110.40976188069389,
      img: "assets/store/store2.jpg",
      title: "Dr Vapor Store (Toko Vape Jogja)",
      deskripsi:
          "Jl. Babarsari No.112A, Kledokan, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281",
    ),
    MarkerMapsModel(
      lat: -7.76821051098267,
      long: 110.39125428189331,
      img: "assets/store/store3.jpg",
      title: "Banderas Vape Squad Gejayan",
      deskripsi:
          "Jl. Affandi, Santren, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281",
    ),
    MarkerMapsModel(
      lat: -7.808700680794253,
      long: 110.40228658374271,
      img: "assets/store/store4.jpg",
      title: "MOREVAPOR GEDONG KUNING",
      deskripsi:
          "Jl. Gedongkuning Jl. Pringgolayan No.148, Pringgolayan, Rejowinangun, Kec. Kotagede, Bantul, Daerah Istimewa Yogyakarta 55171",
    ),
  ];

  Widget liquidPanel(int index) {
    return Container(
      width: 300,
      margin: EdgeInsets.only(right: 10),
      decoration: BoxDecoration(
        color: Color(0xffc7c7c7),
        borderRadius: BorderRadius.circular(18),
      ),
      child: GestureDetector(
        onTap: () {
          Get.to(
            Maps(
              markerData: vapeStore[index],
            ),
          );
        },
        child: Stack(children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(18),
            child: Image.asset(
              vapeStore[index].img,
              fit: BoxFit.fill,
              height: double.infinity,
              width: double.infinity,
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.5),
              borderRadius: BorderRadius.circular(18),
            ),
          ),
          Positioned(
            bottom: 20,
            left: 15,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(
                  Icons.place,
                  size: 18,
                  color: Colors.white,
                ),
                SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      vapeStore[index].title,
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      width: 250,
                      child: Text(
                        vapeStore[index].deskripsi,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 10,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 36),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Store",
            style: TextStyle(
              color: Color(0xffc7c7c7),
              fontWeight: FontWeight.w700,
              letterSpacing: 1,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 180,
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemCount: vapeStore.length,
              itemBuilder: (BuildContext _, int index) {
                return liquidPanel(index);
              },
            ),
          ),
        ],
      ),
    );
  }
}
