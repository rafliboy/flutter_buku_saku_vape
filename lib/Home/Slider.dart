import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';

final List<String> imgList = [
  'assets/gif/dtl.gif',
  'assets/gif/mtl.gif',
];

class Slider extends StatefulWidget {
  @override
  _SliderState createState() => _SliderState();
}

class _SliderState extends State<Slider> {
  final CarouselController _controller = CarouselController();
  int active = 0;

  final List<Widget> imageSliders = imgList
      .map(
        (item) => Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Image.asset(
              item,
              // width: double.infinity,
              height: 400,
              fit: BoxFit.cover,
            ),
          ),
        ),
      )
      .toList();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      color: Colors.white,
      child: Column(
        children: [
          CarouselSlider(
            items: imageSliders,
            options: CarouselOptions(
                viewportFraction: 1,
                enlargeCenterPage: true,
                onPageChanged: (int index, reason) {
                  setState(() {
                    active = index;
                  });
                }),
            carouselController: _controller,
          ),
          Container(
            padding: EdgeInsets.only(bottom: 10, left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(
                imgList.length,
                (index) {
                  return Flexible(
                    child: GestureDetector(
                      onTap: () => _controller.animateToPage(index),
                      child: Container(
                        width: 10,
                        height: 10,
                        decoration: BoxDecoration(
                          color:
                              active == index ? Color(0xff183D60) : Colors.grey,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        margin: EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: 5,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
