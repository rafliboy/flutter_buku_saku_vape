import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:uts/Home/Slider.dart' as sliderPage;
import 'package:uts/LiquidPage.dart';
import 'KategoryWidget.dart';
import 'LiquidWidget.dart';
import 'StoreWidget.dart';
import 'AppHeader.dart' as header;

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe0e0e0),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // appBar
              header.AppHeader(),

              //Slider
              sliderPage.Slider(),

              //kategoryWidget
              Container(
                padding: EdgeInsets.symmetric(
                  vertical: 15,
                ),
                margin: EdgeInsets.only(bottom: 10),
                color: Colors.white,
                child: KategoryWidget(),
              ),

              //LiquidWidget
              Container(
                padding: EdgeInsets.symmetric(
                  vertical: 15,
                ),
                margin: EdgeInsets.only(bottom: 10),
                color: Colors.white,
                child: LiquidWidget(),
              ),

              //storeWidget
              Container(
                padding: EdgeInsets.symmetric(
                  vertical: 15,
                ),
                margin: EdgeInsets.only(bottom: 10),
                color: Colors.white,
                child: StoreWidget(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
