import 'package:flutter/material.dart';
import 'package:get/get.dart';

Map<String, dynamic> uwell = {
  "title": "Caliburn uwell",
  "Dimensi": "110mm*21.2mm*11.6mm",
  "Konstruksi": "Alminium-Alloy",
  "Baterai": "isi ulang baterai Portabel 520mAh",
  "Kapasitas E-Juice": "2ML",
  "Rentang Output watt": "11W",
  "Rentang ouput voltage": "3.2-4.0V",
  "Rentang resistensi": "1.2-1.5ohm",
  "Resistensi Pod Coil": "1.4ohm",
  "img": "assets/vape/uwell.jpg",
};

Map<String, dynamic> kuy = {
  "title": "KUY by movi",
  "Dimensi": "110mm*21.2mm*11.6mm",
  "Konstruksi": "Alminium-Alloy",
  "Baterai": "isi ulang baterai Portabel 520mAh",
  "Kapasitas E-Juice": "2ML",
  "Rentang Output watt": "11W",
  "Rentang ouput voltage": "3.2-4.0V",
  "Rentang resistensi": "1.2-1.5ohm",
  "Resistensi Pod Coil": "1.4ohm",
  "img": "assets/vape/kuy.jpg",
};

Map<String, dynamic> vinci = {
  "title": "VINCI X by voopoo",
  "Size": "117 x 29.5 x 25.3mm",
  "Power Range": "5 - 70W",
  "Capacity": "5.5ml / 2ml (TPD)",
  "Resistance Range": "0.1 - 3.0Ω",
  "Output Voltage": "3.2 - 4.2V",
  "Battery": "Single 18650 (excluded)",
  "Material": "Zinc Alloy + PCTG",
  "Coils": "Suitable for PnP coils",
  "img": "assets/vape/vinci.jpg",
};

Map<String, dynamic> oxva = {
  "title": "OXVA ORIGIN X",
  "Dimension": "135.7*25.0*34.5mm",
  "Weight": "100g",
  "Capacity": "3.2ml",
  "Recommended Watt": "0.2Ω(50-60W) / 1.0Ω(10-15W)",
  "Coil": "Compatible with ALL Unicoil system",
  "Cartridge": "Pod & RBA & 510 tank",
  "Output power": "5-60W",
  "Output voltage": "3.3-4.2V",
  "Battery": "External 18650 battery",
  "Charging current": "2A (Type-C)",
  "Charging voltage": "5V",
  "Display": "0.69 inch OLED Screen",
  "img": "assets/vape/oxva.jpg",
};

List<Map<String, dynamic>> pods = [uwell, kuy];
List<Map<String, dynamic>> mods = [vinci, oxva];

List<List<Map<String, dynamic>>> produk = [pods, mods];

class KategoryDetail extends StatelessWidget {
  final String title;
  final String info;
  final String image;
  final int active;

  KategoryDetail({this.title, this.info, this.image, this.active = 0});

  List<List<Widget>> data() {
    List<List<Widget>> d = [];
    for (var i = 0; i < produk[active].length; i++) {
      List<Widget> newText = [];
      produk[active][i].forEach((key, value) {
        if (key == "title") {
          newText.add(
            Container(
              margin: EdgeInsets.only(bottom: 10),
              child: Text(
                "${value}",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
          );
        } else if (key != "img") {
          newText.add(
            Container(
              margin: EdgeInsets.only(bottom: 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${key} : ",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "${value}",
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        }
      });
      d.add(newText);
    }
    return d;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height / 3.5,
                width: MediaQuery.of(context).size.width,
                color: Color(0xff183D60),
                child: Stack(
                  children: [
                    Align(
                      alignment: AlignmentDirectional.topStart,
                      child: GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 20,
                      right: 80,
                      child: Image.asset(
                        image,
                        height: MediaQuery.of(context).size.height / 4.5,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 7,
                    ),
                    Text(
                      title.toUpperCase(),
                      style: TextStyle(
                        color: Colors.white,
                        letterSpacing: 1,
                        fontSize: 20,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 30,
                          vertical: 30,
                        ),
                        child: Text(
                          info,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "List Mods detail :",
                      style: TextStyle(color: Colors.grey, letterSpacing: 1),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ListView.builder(
                      itemCount: pods.length,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Container(
                                  width: 90,
                                  height: 90,
                                  padding: EdgeInsets.all(10),
                                  child: Image.asset(
                                    produk[active][index]["img"],
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 20,
                                      vertical: 10,
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children:
                                          data()[index].map((e) => e).toList(),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
