import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:uts/Home/Home.dart';

class Splashscreen extends StatefulWidget {
  @override
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // menunggu 1 detik dan akan pergi ke page home
    Future.delayed(Duration(seconds: 1), () {
      Get.off(Home());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: 100,
          height: 100,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(50),
          ),
          child: Center(
            child: Text(
              "Vapor",
              style: TextStyle(
                color: Color(0xff183D60),
                letterSpacing: 1,
                fontSize: 32,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
      backgroundColor: Color(0xff183D60),
    );
  }
}
