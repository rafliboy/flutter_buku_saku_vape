class MarkerMapsModel {
  final String title;
  final String deskripsi;
  final double lat;
  final double long;
  final String img;

  MarkerMapsModel({this.lat, this.long, this.title, this.deskripsi, this.img});
}
