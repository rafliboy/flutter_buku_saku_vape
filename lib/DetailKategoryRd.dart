import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:uts/DetailRd.dart';

class DetailKategoryRd extends StatefulWidget {
  @override
  _DetailKategoryRdState createState() => _DetailKategoryRdState();
}

class _DetailKategoryRdState extends State<DetailKategoryRd> {
  String info =
      "Atomizer adalah alat untuk merubah wujud suatu benda cair ke wujud lain yaitu uap. Jadi penjelasan paling mudahnya atomizer adalah alat untuk merubah liquid vape kita sehingga bisa beruap. Ada banyak jenis dari atomizer ini, jenis-jenis atomizer ini tergantung pada kebutuhan kalian saat vaping. ";

  List<InfoBox> content = [
    InfoBox(
        title: "RDA",
        deskripsi: "",
        page: DetailRd(
          title: "RDA",
          image: "assets/img/rda.png",
          info:
              "RDA adalah singkatan dari Rebuildable Drip Atomizer. RDA adalah perangkat yang mengharuskan pengguna meneteskan e-liquid secara manual ke koil dan sumbu sebelum melakukan vaping.",
        )),
    InfoBox(
        title: "RTA",
        deskripsi: "",
        page: DetailRd(
          title: "RTA",
          image: "assets/img/rta.png",
          info:
              "RTA adalah Alat Penyemprot Tangki Rebuildable. Desain RTA adalah konsep yang sama dengan RDA kecuali bahwa alih-alih meneteskan e-liquid, RTA memiliki tangki yang mengandung e-liquid. Intinya, RTA adalah perangkat yang memungkinkan pengguna untuk membuat gulungan sendiri tetapi tetap menikmati kemudahan tangki vape. Yaitu tidak harus secara manual menetes jus vape setiap empat dari lima hit.",
        )),
    InfoBox(
        title: "RDTA",
        deskripsi: "",
        page: DetailRd(
          title: "RDTA",
          image: "assets/img/rdta.png",
          info:
              "RDTA adalah singkatan dari Rebuildable Dripping Tank Atomizer. Sekilas dari kepanjangannya mungkin kamu akan paham bahwa RDTA merupakan gabungan dari RTA dan RDA yaitu atomizer yang memiliki tank dan menggunakan prinsip bongkar pasang ",
        )),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Stack(
            children: [
              // SizedBox(
              //   height: MediaQuery.of(context).size.height,
              // ),
              Container(
                height: MediaQuery.of(context).size.height / 3.5,
                width: MediaQuery.of(context).size.width,
                color: Color(0xff183D60),
                child: Stack(
                  children: [
                    Align(
                      alignment: AlignmentDirectional.topStart,
                      child: GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 20,
                      right: 80,
                      child: Image.asset(
                        "assets/vape/automizerfull.png",
                        height: MediaQuery.of(context).size.height / 4.5,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 7,
                    ),
                    Text(
                      "Automizer",
                      style: TextStyle(
                        color: Colors.white,
                        letterSpacing: 1,
                        fontSize: 20,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 30,
                          vertical: 30,
                        ),
                        child: Text(
                          info,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        content[0].content(),
                        content[1].content(),
                        content[2].content(),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class InfoBox {
  final String title;
  final String deskripsi;
  final Widget page;
  InfoBox({this.title, this.deskripsi, this.page});

  Widget content() {
    return Expanded(
      child: GestureDetector(
        onTap: () {
          Get.to(page);
        },
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(
              vertical: 5,
              horizontal: 5,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title.toUpperCase(),
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                  textAlign: TextAlign.start,
                ),
                SizedBox(height: 50),
                Container(
                  alignment: Alignment.bottomRight,
                  width: double.infinity,
                  child: Icon(
                    Icons.touch_app,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
