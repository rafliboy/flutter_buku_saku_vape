import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:uts/DetailRd.dart';
import 'package:uts/model/LiquidModel.dart';

class LiquidPage extends StatefulWidget {
  @override
  _LiquidPageState createState() => _LiquidPageState();
}

class _LiquidPageState extends State<LiquidPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              AppHeader(),
              HeaderContent(),
              Content(),
            ],
          ),
        ),
      ),
      backgroundColor: Color(0xffe0e0e0),
    );
  }
}

class AppHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
      decoration: BoxDecoration(
        color: Color(
          0xff183D60,
        ),
      ),
      child: Align(
        alignment: AlignmentDirectional.centerStart,
        child: Row(
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
            // SizedBox(width: 20),
            // Text(
            //   "Liquid",
            //   style: TextStyle(
            //     fontSize: 20,
            //     fontWeight: FontWeight.w700,
            //     color: Colors.white,
            //     letterSpacing: 2,
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}

class HeaderContent extends StatelessWidget {
  String info =
      "Liquid adalah cairan perasa manis yang terbuat dari Vegetable Glycerin (VG), Propylene Glycol (PG), flavour (perasa buah-buahan atau makanan lainnya) dan nikotin. Jika rokok menggunakan tembakau sebagai bahan utamanya, maka vape menggunakan liquid sebagai bahan utamanya. Liquid sendiri dibagi dua yaitu freebase untuk open system atau AIO dan salt nic,untuk close sistem atau juga bisa AIO";

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Container(
              height: 200,
              width: double.infinity,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Stack(
                  children: [
                    Image.asset(
                      "assets/liquid.jpg",
                      width: double.infinity,
                      height: double.infinity,
                      fit: BoxFit.cover,
                    ),
                    Container(
                      width: double.infinity,
                      height: double.infinity,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: [
                            0.5,
                            1,
                          ],
                          colors: [Colors.black.withOpacity(0), Colors.black],
                        ),
                      ),
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Text(
                            "Liquid",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          // Text(
          //   "Liquid",
          //   style: TextStyle(
          //     fontWeight: FontWeight.w600,
          //     fontSize: 18,
          //   ),
          // ),
          SizedBox(height: 10),
          Text(info),
        ],
      ),
    );
  }
}

class Content extends StatelessWidget {
  List<LiquidModel> liquid = [
    LiquidModel(
      title: "Creamy",
      image: "assets/liquid/liquid1.jpg",
      deskripsi:
          "Rasa liquid ini memiliki banyk peminatnya, dikarenkn rasanya yang memang enak, manis dan tersedia dengan bebagai varian rasa, seperti coklat, strawberry, mangga dan lain sebagainya. Dan juga rasa liquid ini juga sangat cocok atau familiar dengan lidah orang Indonesia.",
    ),
    LiquidModel(
      title: "Fruity",
      image: "assets/liquid/liquid2.jpeg",
      deskripsi:
          "Dari namanya kita sudah bisa menebak bahwa liquid ini memiliki rasa buah buahan seperti melon, mangga, anggur, jeruk dan lain sebaginya. Dan untuk harganya liquid ini lebih murah dibandingkan dengan liqiud creamy, tetapi kalau fruitnya produksi dari US seperti contohnya Naked harganya lebih mahal.",
    ),
    LiquidModel(
      title: "JUICY",
      image: "assets/liquid/liquid3.jpg",
      deskripsi:
          "Liquid juicy tidak jauh beda dengan fruity yaitu memiliki citra rasa buah-buahan segar dan mengandung chiler untuk rasa dinginya. Biasanya juicy menghasilkan rasa buah-buahan yang telah dicampur. Seperti kiwi-berry, melon-bery, peach-kiwi, jeruk-sitrus, dan masih banyak lagi.",
    ),
    LiquidModel(
      title: "TOBACCO (RASA ROKOK)",
      image: "assets/liquid/liquid4.jpg",
      deskripsi:
          "Liquid jenis ini akan memberikan sensasi seperti menghisap tembakau. Ya, liquid tobacco merupakan liquid yang memiliki rasa tembakau. Liquid jenis tobacco yang paling terkenal ialah vanila crustad, caramel, dan tobacco strawberry.",
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: GridView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: liquid.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
        ),
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              Get.to(DetailRd(
                title: liquid[index].title,
                info: liquid[index].deskripsi,
                image: liquid[index].image,
              ));
            },
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Image.asset(
                      liquid[index].image,
                      width: double.infinity,
                      height: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: [
                            0.5,
                            1,
                          ],
                          colors: [Colors.black.withOpacity(0), Colors.black],
                        )),
                    child: Align(
                      alignment: Alignment.bottomLeft,
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Text(
                          liquid[index].title,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
